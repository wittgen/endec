#pragma once

#include <cstdint>
#include <type_traits>
#include <concepts>
#include <iostream>
#include <bit>
#include <bitset>

#include "Bitstream.hpp"
#include "BinaryTree.hpp"
#include "interfaces/DecoderOutput.hpp"
#include "interfaces/DataBuffer.hpp"
#include "interfaces/Options.hpp"

namespace itkpix::codec {

    /**
     * @class StreamDecoder
     * @brief Decode a ITK pixel data stream
     * @tparam T Data buffer type
     * @tparam OutPut The type of output processor
     * @tparam Opts Decoder options
     */
    template<itkpix::concepts::DataBuffer T,
            itkpix::concepts::DecoderOutput OutPut>
    class StreamDecoder {
    public:
        StreamDecoder(T const &data_, OutPut &proc, Options options = Options()) :
                data(data_, options.enable_id ? 3 : 0),
                out(proc), options_(options) {}

        ~StreamDecoder() = default;

        /**
         * @fn void StreamDecoder::decode()
         * @brief Decode the ITK pixel data stream
         *
         * This function decodes the ITK pixel data stream by reading the compressed
         * data and generating events for each code word and hit map.
         *
         * @note This function assumes that the data buffer is valid and has enough
         *       data to be processed.
         *
         * @note This function uses lambda functions to simplify the code and improve
         *       readability.
         *
         * @note The function guarantees that the bits_processed() function returns
         *       the correct number of processed bits after the function finishes.
         *
         */
        void decode() {
            uint64_t ccol, tag, ctrl, qrow, intTag;
            bool is_neighbor{false}, is_last{true};
            auto fetch_ccol = [&]() -> bool {
                auto result = data.fetch(ccol, 6);
                if ((ccol >> 3) == 0b111) { // internal tag
                    uint64_t intTag{};
                    result = result && data.fetch(intTag, 5);
                    intTag |= ((ccol & 0b111) << 5);
                    result = result && data.fetch(ccol, 6);
                    out.start_event(intTag);
                }
                return result;
            };
            auto fetch_ctrl = [&]() -> bool {
                auto result = data.fetch(ctrl, 2);
                is_last = (ctrl & 2) == 2;
                is_neighbor = (ctrl & 1) == 1;
                return result;
            };

            auto fetch_hitmap = [&]() -> bool {
                uint64_t hit_map{};
                auto result = data.prefetch(hit_map, 30);
                uint32_t decoded{};
                auto len = BinaryTreeLUT::decode(hit_map, decoded);
                data.advance(len);
                uint64_t tot{};
                auto hits = std::popcount(decoded);
                data.fetch(tot, hits * 4);
                out.add_hit(ccol, qrow, tot);
                return result;
            };

            data.fetch(tag, 8);
            out.start_event(tag);
            while (data.left() > 6) {
                if (is_last) {
                    auto success = fetch_ccol();
                }
                fetch_ctrl();
                if (!is_neighbor) {

                    data.fetch(qrow, 8);
                    qrow&=0xff;
                }
                fetch_hitmap();
            }
        }

        std::size_t bits_processed() const {
            return data.index();
        }

    private:
        static inline constexpr uint64_t bit_mask(std::size_t n) { return (uint64_t(1) << n) - 1; }

        BitStreamReader<T> data;
        OutPut &out;
        const Options options_{};
    };
}