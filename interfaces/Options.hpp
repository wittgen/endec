#pragma once

#include <cstdint>

namespace itkpix::codec {
struct Options {
    bool enable_id = true;
    bool compress_hitmap = true;
    bool enable_tag = true;
    bool enable_tot = true;
    uint8_t chip_id = 0;
    uint8_t events = 1;
    uint8_t orphan_bits = 6;
};
};
