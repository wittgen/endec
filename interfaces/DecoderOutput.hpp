#pragma once

#include <concepts>

namespace itkpix::concepts {
template<class T> concept DecoderOutput = requires(T a, uint16_t b) {
    { a.start_event(b) } -> std::same_as<void>;
    { a.end_event() } -> std::same_as<void>;
    { a.add_hit(b, b, b) } -> std::same_as<void>;
};
}