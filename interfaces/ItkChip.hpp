#pragma once

#pragma once
#include <cstddef>
#include <array>

namespace itkpix::frontend {
struct ItkChip {
    static constexpr std::size_t nCol = 400;
    static constexpr std::size_t nRow = 384;
    static constexpr std::size_t nColInCCol = 8;
    static constexpr std::size_t nRowInQRow = 2;
    static constexpr std::size_t nCCol = nCol / nColInCCol;
    static constexpr std::size_t nQRow = nRow / nRowInQRow;
};
}