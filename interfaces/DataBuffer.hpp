#pragma once
#include <concepts>
namespace itkpix::concepts {

template<typename T>
concept DataBuffer = requires(T t, std::size_t s) {
    { t.size() } -> std::convertible_to<std::size_t>;
    { t[s] } -> std::same_as<uint64_t &>;
};

}