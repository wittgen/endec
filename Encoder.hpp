#pragma once

#include <vector>
#include <iostream>
#include <random>
#include "interfaces/ItkChip.hpp"
#include "BinaryTree.hpp"
#include "Bitstream.hpp"
#include "interfaces/Options.hpp"
#include "HitMap.hpp"
using namespace  itkpix::frontend;
using HitMap = itk::utils::HitMap<itkpix::frontend::ItkChip::nCol,
        itkpix::frontend::ItkChip::nRow, uint16_t>;
namespace itkpix::codec {
using DataArray = std::array<uint64_t, 1<<16>;
using BitStreamArrayWriter = BitStreamWriter<DataArray>;
using BitSreamVectorWriter = BitStreamWriter<std::vector<uint64_t>>;

template<typename Writer = BitStreamArrayWriter,
        typename ChipTraits = ItkChip>
class Encoder : public ChipTraits {
public:
    Encoder(Writer::buffer_type *data,
            Options options = Options{}) :
            _data(data),
            writer_(*data, options.enable_id ? 3 : 1),
            _opts{options} {}
    void new_stream(Writer::buffer_type *data) {
        _data = data;
        writer_ = Writer(*data, _opts.enable_id ? 3 : 1);
    }
    bool add_event(HitMap const &hitmap, bool last = false) {
        if (currEvent == 0) {
            // tag
            //_writer.write(currStream, 8);
            writer_.write(0xff, 8);
            currStream++;
        } else {
            // add internal tag
            uint16_t tag = currEvent | (0xf << 8);
            writer_.write(tag, 11);
        }
        encode_event(hitmap);
        currEvent++;
        if (currEvent == _opts.events || last) {
            if(_opts.enable_id)
                writer_.setHeaders(_opts.chip_id);
            currEvent = 0;
            return false;
        }
        return true;
    }
    std::size_t size() const {
        return writer_.wordSize();
    }

private:
    void encode_event(HitMap const &hitmap) {
        auto hitInQCore = [&](const uint CCol, const uint QRow) -> bool {
            uint col = CCol * ChipTraits::nColInCCol;
            uint row = QRow * ChipTraits::nRowInQRow;

            for (uint pixRow = row; pixRow < row + ChipTraits::nRowInQRow; pixRow++) {
                for (uint pixCol = col; pixCol < col + ChipTraits::nColInCCol; pixCol++) {
                    if (hitmap(pixCol, pixRow)) return true;
                }
            }

            return false;
        };

        auto encode_qcore = [&](const uint nCCol, const uint nQRow) {
            uint col = nCCol * ChipTraits::nColInCCol;
            uint row = nQRow * ChipTraits::nRowInQRow;
            int hits{};
            uint16_t lutIndex = 0x0000;
            std::array<uint8_t, 16> tots{};
            int i = 0;
            for (uint pixRow = row; pixRow < row + ChipTraits::nRowInQRow; pixRow++) {
                for (uint pixCol = col; pixCol < col + ChipTraits::nColInCCol; pixCol++) {
                    if (hitmap(pixCol, pixRow)) {
                        lutIndex |= 0x1 << i;
                        i++;
                        tots[hits++] = (hitmap(pixCol, pixRow) - 1);
                    }
                }
            }
            if(_opts.compress_hitmap) {
                uint32_t encoded;
                auto len = BinaryTreeLUT::encode(lutIndex, encoded);
                writer_.write(encoded, len);
            } else {
                writer_.write(lutIndex, 16);
            }
            if (!_opts.enable_tot) return;
            for (i =0 ;i< hits;i++) {
                writer_.write(tots[i], 4);
            }
        };
        auto hitQCores = std::array<std::array<bool, ChipTraits::nQRow>, ChipTraits::nCCol>{false};
        auto lastQRow = std::array<uint, ChipTraits::nCol>{0};
        for (uint cCol = 0; cCol < ChipTraits::nCCol; cCol++) {
            for (uint qRow = 0; qRow < ChipTraits::nQRow; qRow++) {
                hitQCores[cCol][qRow] = hitInQCore(cCol, qRow);
                if (hitQCores[cCol][qRow]) lastQRow[cCol] = qRow + 1;
            }
        }

        for (uint CCol = 0; CCol < ChipTraits::nCCol; CCol++) {
            if (lastQRow[CCol] == 0) continue;
            writer_.write(CCol + 1, 6);
            int previousQRow = -99999;
            for (uint QRow  = 0; QRow < ChipTraits::nQRow; QRow++) {
                if (!hitQCores[CCol][QRow]) continue;
                uint32_t is_last = (QRow + 1 == lastQRow[CCol]) ? 1 : 0;
                writer_.write(is_last, 1);

                uint32_t neighbor = (QRow == previousQRow + 1) ? 1 : 0;
                writer_.write(neighbor, 1);
                if(neighbor==0) writer_.write(QRow, 8);
                encode_qcore(CCol, QRow);
                previousQRow = QRow;
            }
        }
    }

    Writer writer_;
    Writer::buffer_type *_data;
    uint32_t currEvent{};
    uint32_t currStream{};
    Options _opts{};
};
}