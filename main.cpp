#include "HitMapGenerator.hpp"
#include "Encoder.hpp"
#include "Decoder.hpp"
#include "MemoryWrapper.hpp"
#include "HistogramOutput.hpp"
#include "interfaces/Options.hpp"
using namespace itkpix::codec;
using BitStreamMemWrapper = BitStreamWriter<MemWrapper<uint64_t>>;
using HitMapVec = std::vector<HitMap>;

std::shared_ptr<HitMapVec>  generate_true_maps(unsigned n, float occupancy) {
    using HitMapVec = std::vector<HitMap>;
    std::shared_ptr<HitMapVec> p_maps = std::make_shared<HitMapVec>(n);  ;
    HitMapVec &m = *p_maps;
    ItkHitMapGenerator gen(36);

    for(int i=0;i<n;i++) {
        gen.generate(m[i], occupancy);
    }
    return p_maps;
}

std::vector<std::vector<uint64_t>> encode_events(Options opt, HitMapVec const &vec) {
    std::vector<std::vector<uint64_t>> output;
    std::vector<uint64_t> event;
    Encoder<BitSreamVectorWriter> encoder(&event, opt);
    for(int i;i<vec.size();i++) {
        bool last = encoder.add_event(vec[i]);
        if(last) {
            output.emplace_back(std::move(event));
            encoder.new_stream(&event);
        }
    }
    return output;
}


int main() {
    auto hitmaps = generate_true_maps(128, 0.1);
    auto output = encode_events(Options { .chip_id = 3, .events = 64}, *hitmaps);
    for(auto &v: output) {
        MemWrapper<uint64_t> dstream(v.data(), v.size());
        HistogramOutput h(64);
        StreamDecoder<MemWrapper<uint64_t>, HistogramOutput> decoder(dstream, h);
        decoder.decode();
    }



}
