#pragma once
#include <cstdlib>
#include <vector>
#include <iostream>

namespace itk::utils {
template <size_t Cols, size_t Rows, typename DataType>
class HitMap {
public:
    HitMap() = default;
    ~HitMap() = default;

    HitMap(const HitMap &other) : data_(other.data_) {}

    HitMap(HitMap &&other) noexcept : data_(std::move(other.data_)) {}

    HitMap &operator=(const HitMap &other) {
        if (this != &other) {
            data_ = other.data_;
        }
        return *this;
    }

    HitMap &operator=(HitMap &&other) noexcept {
        if (this != &other) {
            data_ = std::move(other.data_);
        }
        return *this;
    }

    const DataType & operator()(size_t col, size_t row) const {
        return data_[row * Cols + col];
    };

    DataType & operator()(size_t col, size_t row) {
        if(row * Cols + col < data_.size())
        return data_[row * Cols + col];
        else {
            std::cout << std::hex << "**** " << col << " " << row << "\n";
            abort();
        }
    };

    void fill_all(DataType value) {
        std::fill(data_.begin(), data_.end(), value);
    }
    void clear() {
        fill_all(DataType(0));
    }

private:
    std::vector<DataType> data_{std::vector<DataType>(Rows * Cols)};
};

}