#pragma once

#include <cstddef>

template<typename T>
class MemWrapper {
public:
    MemWrapper(T *start, std::size_t size) : _start(start), _size(size
    ) {}


    using value_type = T;

    std::size_t size() const {
        return _size;
    }

    T &operator[](std::size_t index) {
        return _start[index];
    }

    const T &operator[](std::size_t index) const {
        return _start[index];
    }

    T *begin() {
        return _start;
    }

    T *end() {
        return _start + _size;
    }

    const T *cbegin() {
        return _start;
    }

    const T *cend() {
        return _start + _size;
    }

private:
    T *_start;
    std::size_t _size;
};