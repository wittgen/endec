#pragma once

#include <cstdint>
#include "interfaces/ItkChip.hpp"
#include "HitMap.hpp"
using HitMap = itk::utils::HitMap<itkpix::frontend::ItkChip::nCol,
        itkpix::frontend::ItkChip::nRow, uint16_t>;

class HistogramOutput {
public:
    HistogramOutput(size_t n) : n_(n), hitmap_(n), current_(hitmap_[0]) {}
    void start_event(uint16_t tag) {
        current_ = hitmap_[index_];
        index_++;
    }
    void end_event() {}
    void add_hit(uint16_t col, uint16_t row, uint16_t tot) {
        current_(col, row) = tot - 1;
    }
private:
    size_t n_;
    std::vector<HitMap> hitmap_;
    size_t index_{};
    HitMap &current_;

};
