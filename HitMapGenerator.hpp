#pragma once

#include "interfaces/ItkChip.hpp"
#include "HitMap.hpp"
#include <cstdint>
#include <random>

using HitMap = itk::utils::HitMap<itkpix::frontend::ItkChip::nCol,
        itkpix::frontend::ItkChip::nRow, uint16_t>;

using namespace itkpix::frontend;
template<class T> concept DataOut =
requires(T a, uint16_t b) {
    {a.new_event(b)};
    {a.add_hit(b, b, b)};
};



class DataProc {
public:
    void new_event(uint16_t tag) {

    }
    void add_git(uint16_t x, uint16_t y, uint16_t tot) {

    }

};


template<typename T>
class HitMapGenerator : public T {
    using T::nCol;
    using T::nRow;
public:
    explicit HitMapGenerator(uint32_t seed) :  seed_(seed) {
        generator_.seed(seed_);
    }

    void set_all(HitMap &hitmap, uint8_t value) {
        hitmap.fill_all(value);
    }
    void generate(HitMap &hitmap, float occupancy) {
        for (int col = 0; col < nCol; col++) {
            for (int row = 0; row < nRow; row++) {
                float hitprob = hitProb_(generator_);
                if (hitprob < occupancy) {
                    hitmap(col, row) = totProb_(generator_);
                } else hitmap(col, row)=0;
            }
        }
    }
private:
    uint32_t seed_{};
    std::mt19937 generator_{seed_};
    std::uniform_real_distribution<float> hitProb_;
    std::uniform_int_distribution<> totProb_{1, 15};

};

using ItkHitMapGenerator = HitMapGenerator<ItkChip>;

